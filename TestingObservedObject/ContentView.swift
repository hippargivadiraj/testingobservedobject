//
//  ContentView.swift
//  TestingObservedObject
//
//  Created by Leadconsultant on 11/28/19.
//  Copyright © 2019 Leadconsultant. All rights reserved.
//

import SwiftUI

class Order: ObservableObject {
    @ Published var items = [String]()
}

struct ContentView: View {

    @ObservedObject var order = Order()
    
    var body: some View {
        NavigationView{
        VStack{
            Spacer()
        Button("Add Items" ){
            self.order.items.append("ITEM 1")
        }
            List(self.order.items, id: \.self) { string in            Text(string)
                
            }
        }.navigationBarTitle("Listing Items")
    }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
